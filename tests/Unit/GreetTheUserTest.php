<?php

use Tuto\GreetTheUser\Greeter;

test('can greet a user', function () {
    expect((new Greeter())->greet('World'))->toBe('Hello World!');
});

# How to use xdebug with docker and codium to debug tests

## Installation

```sh
composer install
```

## Usage

1. Configure codium:
    1. click on *run & debug*
    1. click on *create a launch.json file*
    1. set the `pathMappings` property:

        ```json
        {
            "version": "0.2.0",
            "configurations": [
                {
                    "name": "Listen for Xdebug",
                    "type": "php",
                    "request": "launch",
                    "port": 9003,
                    "pathMappings": {
                        "/srv/app": "${workspaceFolder}"
                    }
                }
            ]
        }
        ```

1. Choose the configuration *Listen for xdebug* and click on *Start Debugging (F5)*

1. Start containers:

    ```sh
    docker compose --file docker/compose.yaml up --detach
    ```

1. Add a breakpoint by clicking on the line number on the left

1. Run tests:

    ```sh
    docker compose --file docker/compose.yaml exec application vendor/bin/pest
    ```

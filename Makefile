DOCKER_COMPOSE = docker compose --file docker/compose.yaml
APPLICATION_EXEC = $(DOCKER_COMPOSE) exec application

.PHONY: test
test: ## Run tests
	$(APPLICATION_EXEC) vendor/bin/pest

.PHONY: start
start: ## Start the application
	$(DOCKER_COMPOSE) up --detach --remove-orphans

.PHONY: stop
stop: ## Stop the application
	$(DOCKER_COMPOSE) stop

.PHONY: application-bash
application-bash: ## Open a terminal in the container
	$(APPLICATION_EXEC) bash

.DEFAULT_GOAL := help
.PHONY: help
help: ## Describe targets
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
